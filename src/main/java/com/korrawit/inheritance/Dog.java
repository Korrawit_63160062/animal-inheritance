/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.inheritance;

/**
 *
 * @author DELL
 */
public class Dog extends Animal {

    public Dog(String name, String color) {
        super(name, color, 4);
        System.out.println("Dog created");
    }

    public void walk() {
        super.walk();
        System.out.println("Dog: " + name + " walk with " + numberOfleg + " legs");
    }

    public void speak() {
        super.speak();
        System.out.println("Dog: " + name + " speak > Box Box!!!");
    }
}
