/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.inheritance;

/**
 *
 * @author DELL
 */
public class Animal {

    protected String name;
    protected int numberOfleg = 0;
    private String color;

    public Animal(String name, String color, int numberOfLeg) {
        System.out.println("Animal created");
        this.name = name;
        this.color = color;
        this.numberOfleg = numberOfLeg;
    }

    public void walk() {
        System.out.println("Animal walk");
    }

    public void speak() {
        System.out.println("Animal speak");
        System.out.println("name: " + this.name + " color: " + this.color + " numberOfLeg: " + this.numberOfleg);
    }
    
}
