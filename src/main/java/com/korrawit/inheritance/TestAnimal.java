/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.inheritance;

/**
 *
 * @author DELL
 */
public class TestAnimal {

    public static void main(String[] args) {

        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "Black&White");
        dang.speak();
        dang.walk();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();

        Duck gabgab = new Duck("GabGab", "Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        Duck som = new Duck("Som", "Orange");
        som.speak();
        som.walk();
        som.fly();
        
        Dog mome = new Dog("Mome", "Black&White");
        mome.speak();
        mome.walk();
        
        Dog bat = new Dog("Bat", "Black&White");
        bat.speak();
        bat.walk();
        
        Dog to = new Dog("To", "Black&White");
        to.speak();
        to.walk();
        
        
    }
}
